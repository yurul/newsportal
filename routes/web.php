<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'newsportal/1.0.0'], function () use ($router) {
    $router->get('categories', 'CategoryController@index');
		$router->post('categories', 'CategoryController@store');
		$router->get('categories/{category_id}', 'CategoryController@show');
		$router->put('categories/{category_id}', 'CategoryController@update');
		$router->delete('categories/{category_id}', 'CategoryController@destroy');

		$router->get('categories/{category_id}/news', 'NewsController@index');
		$router->post('categories/{category_id}/news', 'NewsController@store');
		$router->get('categories/{category_id}/news/{news_id}', 'NewsController@show');
		$router->put('categories/{category_id}/news/{news_id}', 'NewsController@update');
    $router->patch('categories/{category_id}/news/{news_id}', 'NewsController@patch');
		$router->delete('categories/{category_id}/news/{news_id}', 'NewsController@destroy');
    $router->post(
      'categories/{category_id}/news/{news_id}/changeCategory',
      'NewsController@changecategory'
     );
     $router->post(
       'categories/{category_id}/news/countNews',
       'NewsController@countNews'
      );

    $router->post('users', 'UserController@store');
    $router->post('users/changeRole', 'UserController@changeRole');
});

	$router->post('auth/token', 'TokenController@getToken');
