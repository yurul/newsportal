<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\News;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	       Category::truncate();
         News::truncate();
         User::truncate();
         DB::table('scopes')->truncate();

         $categories=factory(Category::class, 3)
		        ->create()
		        ->each(function($category){
				          $quantity=rand(5,20);
				          $category->news()->saveMany(
                    factory(News::class, $quantity)->make()
                  );
			      });

        $hash = app('hash');
        User::create([
          'email' => 'admin@admin',
          'password' => $hash->make('admin'),
          'scope_id' => 3,
          'access_token'=>str_random(32)
        ]);

        DB::table('scopes')->insert([
            'name' => 'user'
        ]);

        DB::table('scopes')->insert([
          'name' => 'editor'
        ]);

        DB::table('scopes')->insert([
          'name' => 'admin'
        ]);
    }
}
