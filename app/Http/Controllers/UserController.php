<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\ResponseHelper;
use Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct(){
        $this->middleware('auth',[
          'only'=>['changeRole']
        ]);
     }

    public function store(Request $request){

       $validator = Validator::make($request->post(),[
         'email' => 'required|email|max:250|unique:users,email',
         'password' => 'required|string|min:3|max:50'
       ]);

       if($validator->fails()){
         return ResponseHelper::createResponse($validator->errors()->first(), 400, false);
       }

       $hash = app('hash');

       $user = User::create([
              'email' => $request->post('email'),
              'password' => $hash->make($request->post('password')),
              'scope_id' => 1,
              'access_token'=>str_random(32)
       ]);

       return ResponseHelper::createResponse($user, 201);
    }

    public function changeRole(Request $request){

      if (!Gate::allows('access','admin')) {
        return ResponseHelper::createResponse('Not authorized', 401, false);
      }

      $validator = Validator::make($request->all(),[
        'role' =>'exists:scopes,name',
        'email'=>'exists:users,email'
      ]);

      if($validator->fails()){
        return ResponseHelper::createResponse(
          $validator->errors()->first(),
          400,
          false
        );
      }

      $user=User::where('email','=', $request->input('email'))->get()->first();
      if(!$user){
          return ResponseHelper::createResponse('User not found', 400, false);
      }

      $scope = DB::table('scopes')->where('name', $request->input('role'))->first();
      $user->scope_id = $scope->id;
      $user->save();

      return ResponseHelper::createResponse($user, 200);
    }

}
