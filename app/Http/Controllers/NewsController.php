<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Category;
use App\News;
use App\ResponseHelper;
use Validator;

class NewsController extends Controller
{

  public $defaultRules = [
    'title' => 'required|string|max:250',
    'description' => 'required|string|max:5000'
  ];

  public function __construct(){
     $this->middleware('checkCategoryId');
     $this->middleware('checkNewsId', ['except'=>[
        'index',
        'store',
        'countNews'
     ]]);

     $this->middleware('auth',[
       'except'=>['index','show','countNews']
     ]);
  }

   public function index(Request $request, $category_id){

     $search = $request->input('search', false);
     $offset = $request->input('offset', '0');
     $limit = $request->input('limit', '10');
     $limit=($limit>50)? 50: $limit;

     $category=Category::find($category_id);

     $news=$category->news()->take($limit)
        ->skip($offset)
        ->orderBy('id','DESC')
        ->when($search, function ($query, $search) {
              return $query->where('title', 'like', $search.'%');
        })
        ->get();

     return ResponseHelper::createResponse($news, 200);
   }

   public function store(Request $request, $category_id){

     if (!Gate::allows('access','admin')) {
       return ResponseHelper::createResponse('Not authorized', 401, false);
     }

     $rules = $this->getRulesFor(['title', 'description']);
     $validator = Validator::make($request->post(), $rules);

     if($validator->fails()){
        return ResponseHelper::createResponse(
          $validator->errors()->first(),
          400,
          false
        );
     }

     $news = News::create([
       'title' => $request->post('title'),
       'description' => $request->post('description'),
       'category_id'=> $category_id
     ]);

     return ResponseHelper::createResponse($news, 201);
   }

   public function show(Request $request, $category_id, $news_id){

     $category=Category::find($category_id);
     $news=$category->news()->where('id', $news_id)->get();

     return ResponseHelper::createResponse($news->first(), 200);

   }

   public function update(Request $request, $category_id, $news_id){

      if (!Gate::allows('access','admin')) {
        return ResponseHelper::createResponse('Not authorized', 401, false);
      }

      $rules = $this->getRulesFor($request->post());
      $validator = Validator::make($request->post(), $rules);

      if($validator->fails()){
         return ResponseHelper::createResponse(
           $validator->errors()->first(),
           400,
           false
         );
      }

      $news = News::find($news_id);
      $news->title = $request->input('title');
      $news->description = $request->input('description');
      $news->save();

      return ResponseHelper::createResponse($news, 201);
   }

   public function patch(Request $request, $category_id, $news_id){

     if (!Gate::allows('access','admin')) {
       return ResponseHelper::createResponse('Not authorized', 401, false);
     }

     $rules = $this->getRulesFor($request->all());
     $validator = Validator::make($request->all(), $rules);

     if($validator->fails()){
        return ResponseHelper::createResponse(
          $validator->errors()->first(),
          400,
          false
        );
     }

     $news = News::find($news_id);

     $news->title = $request->input('title') ?
        $request->input('title'):
        $news->title;
     $news->description = $request->input('description') ?
        $request->input('description'):
        $news->description;
     $news->save();

     return ResponseHelper::createResponse($news, 201);
   }

   public function destroy($category_id, $news_id){

     if (!Gate::allows('access','admin')) {
       return ResponseHelper::createResponse('Not authorized', 401, false);
     }

     $category=Category::find($category_id);
     $news=$category->news()->where('id', $news_id)->get();
     $news->first()->delete();

     return ResponseHelper::createResponse(
       'News was successfully deleted',
        200
     );
   }

   public function changecategory(Request $request, $category_id, $news_id){

     if (!Gate::allows('access','admin|editor')) {
       return ResponseHelper::createResponse('Not authorized', 401, false);
     }

     $validator = Validator::make($request->post(), [
       'new_category_id'=>'required|exists:categories,id'
     ]);

     if($validator->fails()){
        return ResponseHelper::createResponse(
          $validator->errors()->first(),
          400,
          false
      );
    }

     $news = News::find($news_id);
     $news->category_id = $request->input('new_category_id');
     $news->save();

     return ResponseHelper::createResponse(
       'Category was changed',
        200
     );
   }

   public function countNews($category_id){
     $count = News::where('category_id','=', $category_id)->count();
     return ResponseHelper::createResponse(
       $count,
      200
     );

   }

   private function getRulesFor($fields){

     $rules=[];
     foreach($this->defaultRules as $key => $value){
        if(in_array($key, $fields)){
          $rules[$key] = $value;
        }
     }
     return $rules;
   }

}
