<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\ResponseHelper;

class TokenController extends Controller
{

    public function getToken(Request $request)
    {

      $email=$request->input('email');
      $password=$request->input('password');

      $user = User::where('email', $email)->first();
      $hash = app('hash');

      if($user && $hash->check($password, $user->password)){
          $scope = DB::table('scopes')->where('id', $user->scope_id)->first();
          $token=$user->access_token;
          $data= [
            "access_token"=> $token,
            "scope"=> $scope->name
          ];

          return ResponseHelper::createResponse($data, 200);
      }
      return ResponseHelper::createResponse('Not authorized', 401, false);
    }


}
