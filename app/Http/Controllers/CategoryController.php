<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Category;
use App\ResponseHelper;
use Validator;

class CategoryController extends Controller
{

   public function __construct(){
      $this->middleware('checkCategoryId',['only'=>[
        'show',
        'update',
        'destroy'
      ]]);

      $this->middleware('auth',['only'=>[
        'store','update','destroy'
      ]]);
   }

   public function index(Request $request){
     
     $offset = $request->input('offset', '0');
     $limit = $request->input('limit', '10');
     $limit=($limit>50)? 50: $limit;

     $categories = Category::take($limit)
        ->skip($offset)
        ->orderBy('id','DESC')
        ->get(['id','name']);

     return ResponseHelper::createResponse($categories, 200);
   }

   public function store(Request $request){

      if (!Gate::allows('access','admin')) {
        return ResponseHelper::createResponse('Not authorized', 401, false);
      }

      $validator = Validator::make($request->post(),[
        'name' => 'required|string|max:250|unique:categories,name'
      ]);

      if($validator->fails()){
        return ResponseHelper::createResponse($validator->errors()->first(), 400, false);
      }

      $name=$request->post('name');
      $category = Category::create(['name' => $name]);

      return ResponseHelper::createResponse($category, 201);
   }

   public function show($category_id){

     $category=Category::find($category_id);
     return ResponseHelper::createResponse($category, 201);
   }

   public function update(Request $request, $category_id){

      if (!Gate::allows('access','admin')) {
        return ResponseHelper::createResponse('Not authorized', 401, false);
      }

     $validator = Validator::make($request->all(),[
       'name' => 'required|string|max:250|unique:categories,name'
     ]);

     if($validator->fails()){
       return ResponseHelper::createResponse($validator->errors()->first(), 400, false);
     }

     $category=Category::find($category_id);
     $category->name = $request->input('name');
     $category->save();

     return ResponseHelper::createResponse($category, 200);
   }

   public function destroy($category_id){

     if (!Gate::allows('access','admin')) {
       return ResponseHelper::createResponse('Not authorized', 401, false);
     }
     $category=Category::find($category_id);
     $category->delete();

     return ResponseHelper::createResponse(
       'Category was successfully deleted',
        200
     );
   }


}
