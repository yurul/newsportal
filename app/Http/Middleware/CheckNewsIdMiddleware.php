<?php

namespace App\Http\Middleware;

use Closure;
use App\News;
use App\ResponseHelper;

class CheckNewsIdMiddleware
{

    public function handle($request, Closure $next)
    {
        $news_id=$request->route()[2]['news_id'];
        $news=News::find($news_id);
        if(!$news){
          return ResponseHelper::createResponse(
            'News not found',
             404,
             false
          );
        }

        return $next($request);
    }
}
