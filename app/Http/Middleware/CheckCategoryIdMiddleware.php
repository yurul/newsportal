<?php

namespace App\Http\Middleware;

use Closure;
use App\Category;
use App\ResponseHelper;

class CheckCategoryIdMiddleware
{

    public function handle($request, Closure $next)
    {
        $category_id=$request->route()[2]['category_id'];
        $category=Category::find($category_id);
        if(!$category){
          return ResponseHelper::createResponse(
            'Category not found',
             404,
             false
          );
        }

        return $next($request);
    }
}
