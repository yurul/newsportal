<?php

namespace App;


class ResponseHelper
{

    public static function createResponse($data, $statusCode, $success=true){

        return response()->json([
            'success' => $success,
            'data' => $data
          ],
          $statusCode
        );

    }

}
