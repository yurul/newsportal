<?php

namespace App\Policies;

use App\User;

class ApiPolicy
{

    public function allowAction(User $user, $scopes)
    {
        $scope = $user->scope();
        $roles = explode('|', $scopes);
        return in_array($scope, $roles);
    }

}
