<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $table = 'news';

    protected $fillable = [
        'title', 'description','category_id'
    ];

    protected $visible = [
        'id', 'title', 'description', 'category_id'
    ];

    public function post(){
        return $this->belongsTo('App\Category');
    }

}
